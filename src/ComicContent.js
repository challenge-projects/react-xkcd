import React, {useEffect, useState} from 'react';
import ComicInfo from './ComicInfo';
import landinImg from './loader.gif';

const ComicContent = () =>{
    const numRandom = () => Math.floor(Math.random() * Math.floor(100));

    useEffect(() =>{
        console.log('Use Efect');
        getComic(numRandom());
    },[])

    const [comicImg, setComicImg] = useState(landinImg);
    const [comicImgAlt, setComicImgAlt] = useState('Imagen de carga');
    const [comicTitle, setComicTitle] = useState('Comic');
    
    const getComic = async (comicId) =>{
        const API = `https://cors-anywhere.herokuapp.com/https://xkcd.com/${comicId}/info.0.json`;
        const comic = await fetch(API);
        const {alt, img, title} = await comic.json();
        
        setComicImgAlt(alt);
        setComicImg(img);
        setComicTitle(title);
    }

    const initialize = () =>{
        clean();
        getComic(numRandom());
    };

    const clean = () =>{
        const getAllStar = document.querySelectorAll('.comic__star');
        const comicPhrase = document.getElementById('comicPhrase');
        const comicButton = document.getElementById('comicButton');
        
        comicPhrase.classList.add('comic__title--inactive');
        comicButton.classList.add('comic__button--inactive');
    
        getAllStar.forEach((star) =>{
            star.classList.remove('comic__star--checked');
        });
    }
    return(
        <div className="container-comic">
            <div className="comic">
                <img src={comicImg} alt={comicImgAlt} id="comicImg" className="comic__img" />
                <ComicInfo title = {comicTitle}/>
            </div>
            <button id="comicButton" className="comic__button comic__button--inactive" onClick={initialize}><i className="fas fa-sync-alt"></i></button>
        </div>
    )
}

export default ComicContent