import React from 'react';
import ComicRating from './ComicRating';
import PropTypes from 'prop-types';

const ComicInfo = ({title}) =>{

    return(
        <div className="comic__info">
            <h1 id="comicTitle" className="comic__title">{title}</h1>
            <ComicRating />
            <h2 id="comicPhrase" className="comic__title comic__title--inactive">Calificación</h2>
        </div>
    )
};


ComicInfo.propsTypes={
    title: PropTypes.string.isRequired,
}

export default ComicInfo