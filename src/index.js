import React from 'react'
import ReactDOM from 'react-dom'
import ComicContent from './ComicContent';
import './index.css';

const divRoot = document.getElementById('root');

ReactDOM.render(<ComicContent />, divRoot);