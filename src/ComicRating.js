import React from 'react';


const ComicRating = () =>{
    const arrPhrases = ['Oops!', 'Si, puede mejorar!', 'No está mal!', 'Grandioso!', 'Excelente!'];
    const score = (e) =>{
        const countStar = parseInt(e.target.dataset.starValue);
        const getAllStar = document.querySelectorAll('.comic__star');
        const comicPhrase = document.getElementById('comicPhrase');
        const comicButton = document.getElementById('comicButton');
        
        getAllStar.forEach((star, key) =>{
            if(( key + 1 ) <= countStar){
                star.classList.add('comic__star--checked');
            }else{
                star.classList.remove('comic__star--checked');
            }
        })
        comicPhrase.innerText = arrPhrases[(countStar - 1)];
        comicPhrase.classList.remove('comic__title--inactive');
        comicButton.classList.remove('comic__button--inactive');
    }


    return (
        <div className="comic__rating">
            <span className="comic__star"><i onClick={score} className="fas fa-star" data-star-value="1" aria-label="Estrella de valor: 1"></i></span>
            <span className="comic__star"><i onClick={score} className="fas fa-star" data-star-value="2" aria-label="Estrella de valor: 2"></i></span>
            <span className="comic__star"><i onClick={score} className="fas fa-star" data-star-value="3" aria-label="Estrella de valor: 3"></i></span>
            <span className="comic__star"><i onClick={score} className="fas fa-star" data-star-value="4" aria-label="Estrella de valor: 4"></i></span>
            <span className="comic__star"><i onClick={score} className="fas fa-star" data-star-value="5" aria-label="Estrella de valor: 5"></i></span>
        </div>
    )
}

export default ComicRating;